# Exercice openbio 💉 

[Open Bio](https://www.data.gouv.fr/fr/datasets/open-bio-base-complete-sur-les-depenses-de-biologie-medicale-interregimes/) est une base de données ouverte sur les consommations et dépenses des actes de biologie médicale. Ces données sont une vue aggrégée des données individuelles présentes dans le [Système National des Données de Santé](https://documentation-snds.health-data-hub.fr/). Chaque ligne donne donc le nombre d'actes et le remboursement correspondant pour une catégorie de la population (ex : nombre d'actes d'histopathologies biopsies uniques ou multiples non individualisées préscrits par un médecin généraliste pour les femmes de 20 à 39 ans en 2018, en Auvergne Rhône-Alpes).

## Problématique, rapport descriptif 🚀

On considère la base openbio fournit dans le dossier `data` (sous-échantillonnée aux années 2018, 2019), ainsi que le schéma et les nomenclatures des données présents dans le dossier `ressources`.

Le but est d'effectuer un rapport descriptif (`.Rmd` ou `.ipynb`) sur ces données en explorant différents axes d'analyse (ex: temporel, precripteurs, ...).

Votre rapport devra être illustré à l'aide de graphiques et de commentaires sur les résultats présentés. Le rapport doit pouvoir se présenter en une quinzaine de minutes et détailler votre démarche d'analyse. 

Mener en premier lieu le sujet numéro 1. Le suejt numéro 2 peut vous donner des pistes pour aborder la base.

### 1 - Sujet principal d'analyse : Le dosage des hormones thyroïdiennes

Essayer de retrouver les chiffres de l'analyse des différents types de dosage des hormones thyroïdiennes mené dans le [rapport des propositions de l'assurance maladie pour 2021](https://assurance-maladie.ameli.fr/sites/default/files/2020-07_rapport-propositions-pour-2021_assurance-maladie_1.pdf), p.78/79.

### 2 - Autres questions métiers types

Des personnes du métier ne connaîssant ni les colonnes, ni les codes des nomenclatures, aimeraient pouvoir avoir des réponses à des questions simples sur ces données grâce à votre rapport : 

- Quel est le nombre de microbiologie courante (groupe) prescrites par un pneumologue en 2018 en Auvergne-Rhône-Alpes ?

- Quel est l'acte avec le montant total le plus remboursé chez les femmes en OCCITANIE en 2019 ? 

- Dans quelle région a-t-on fait le plus de test RT PCR (tous virus confondus en 2018) ? 

# Disclaimer

Ce problème est très ouvert et potentiellement chronophage : Y consacrer de 3 à 4 heures (6 heures grand maximum).

Nous **n'attendons pas** de description exhaustive, simplement une capacité à illustrer des données de santé riches et complexes par une analyse quantitative simple.

Il sera bienvenu de simplifier le problème en ciblant une catégorie particulière d'actes par exemple.

Nous attendons une présentation de 10-15min d'un rapport au format notebook (`.Rmd` (R) ou `.ipynb` (jupyter notebook)) expliquant votre démarche d'exploration et d'analyse des données.
